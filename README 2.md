# PDF Generator
Build the docker image that will be used to generate the PDF
```
./docker/build.sh
```

Generate the PDF from a markdown file
```
./docker/_gene_rate.sh tuto/example.md
```

The PDF file will be generated next to the markdown file
```
ls -l tuto/example.pdf
```