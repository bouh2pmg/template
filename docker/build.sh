#!/usr/bin/env bash

set -e

cd $(dirname $0)/..
ROOTDIR="$(pwd)"
cd - > /dev/null

cd "${ROOTDIR}"
rm -rf docker/tools
mkdir docker/tools

tar --exclude .git --exclude docker -cf - . | tar -xf - -C docker/tools

cd "${ROOTDIR}/docker"
wget -c "https://github.com/jgm/pandoc/releases/download/2.2.2/pandoc-2.2.2-1-amd64.deb"
docker image build -t epitech/template .
rm -rf tools pandoc-2.2.2-1-amd64.deb

# docker container run --rm -it epitech/template